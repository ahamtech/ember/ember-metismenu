/* jshint node: true */
'use strict';

module.exports = {
  name: 'ember-metismenu',

  included: function(app, parentAddon) {
    this._super.included(app);

    app.import('node_modules/metismenu/dist/metisMenu.js');
    app.import('node_modules/metismenu/dist/metisMenu.css');
  }

};
